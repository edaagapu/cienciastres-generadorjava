import os

diccionario = {"contenedor":"JPanel","etiqueta":"JLabel","campo":"JTextField","ventana":"JFrame","boton":"JButton"}
prefijo = {"contenedor":"pnl","etiqueta":"lbl","campo":"txt","ventana":"vnt","boton":"btn"}
importacion = {"contenedor":"javax.swing.JPanel","etiqueta":"javax.swing.JLabel", "campo":"javax.swing.JTextField", "ventana":"javax.swing.JFrame", "boton":"javax.swing.JButton"}

archivo = open("Plantilla","r")
clases = archivo.read().split("--")
clases.pop(0)
caracteristicas = []
for i in clases:
    caracteristicas.append(i.split("\n"))
clase = []
atributos = []

propiedades = []
for i in caracteristicas:
    prop = i[0].split("/")
    clase.append(prop[0].split(" "))
    if (len(prop)>1):
        propiedades.append([prefijo[clase[len(clase)-1][0]].capitalize()+clase[len(clase)-1][1].capitalize(),prop[1][1:len(prop[1])-1].split(",")])
        os.system("mkdir "+propiedades[len(propiedades)-1][0])
    temporal = []
    for j in i[1:len(i)-1]:
        var = j.split("/")        
        temporal.append(var[0].split(":"))
        if (len(var)>1):
            propiedades.append([prefijo[temporal[len(temporal)-1][0][1:]]+temporal[len(temporal)-1][1].capitalize(),var[1][1:len(var[1])-1].split(",")])    
    atributos.append(temporal[:])
carpeta = ""
for i in propiedades:
    if (i[0][0].isupper()):
        carpeta = i[0] 
    prp = open(carpeta + "/" +i[0]+".prop","w")
    for j in i[1]:
        prp.write(j+"\n")
    prp.close()

imp = []
for i in clase:
    imp.append(i[0])
for i in atributos:
    for j in i:
        if not (len(j[0])<=1):
            imp.append(j[0][1:])
importar = set(imp)
constructor = ""
for i in range(len(clase)):
    constructor = ""
    f = open(clase[i][1]+".temp","w")
    f.write("nombre:"+prefijo[clase[i][0]].capitalize()+clase[i][1].capitalize())
    f.write("|")    
    f.write("atributos:")
    for j in range(len(atributos[i])):
        if (atributos[i][j][0][0]=="+"):
            f.write("public ")
        elif (atributos[i][j][0][0]=="-"):
            f.write("private ")
        else:
            f.write("protected ")
        f.write(diccionario[atributos[i][j][0][1:]]+" "+prefijo[atributos[i][j][0][1:]]+atributos[i][j][1].capitalize()+";\n\t")
        constructor = constructor + prefijo[atributos[i][j][0][1:]]+atributos[i][j][1].capitalize() + "= new "+diccionario[atributos[i][j][0][1:]]+"();\n\t\t"
        if prefijo[atributos[i][j][0][1:]]=="pnl":
            constructor = constructor + prefijo[atributos[i][j][0][1:]]+atributos[i][j][1].capitalize() + ".setLayout(null);\n\t\t"
    f.write("|")
    f.write("importaciones:")
    for j in importar:
        f.write("import "+importacion[j]+";\n")
    f.write("|construir:")
    f.write(constructor)    
    f.close()
