#!/usr/bin/env python3

from jinja2 import Template
import os

def ambiente():
    os.system("mkdir Clases")
    os.system("ls | grep '.temp' > archivos.txt")

def construccion(nombre):
    constructor = ""
    ar = open(nombre+".const","r")
    constructor =constructor + ar.read()
    return constructor

ambiente()
clases = open('archivos.txt','r')
gen = clases.read().split("\n")
gen.pop()

tmp = open('template.java','r')
template= Template(tmp.read())

for i in gen:
    archivo = open(i,'r')
    leer = archivo.read().split("|")
    resultado = []
    for j in leer:
        resultado.append(j.split(":"))
    for j in resultado:
        if(j[0]=="nombre"):
            name = j[1]
        elif(j[0]=="atributos"):
            atrib = j[1]
        elif(j[0]=="importaciones"):
            imp = j[1]
        elif(j[0]=="construir"):
            cntrs = j[1]
    f = open("Clases/"+name+'.java','w')
    #nombre,atributos,importaciones,construir
    cntrs = cntrs + construccion(name)
    msg = template.render(nombre=name,atributos=atrib, importaciones=imp,construir=cntrs)
    f.write(msg)
    f.close()
