import java.awt.Color;
{{importaciones}}

public class {{nombre}} extends JFrame {
    private JPanel contentPane;
    {{atributos}}

    public {{nombre}}(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        contentPane = new JPanel();
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        {{construir}}    
    }

    public static void main(String[] args){
        {{nombre}} prueba = new {{nombre}}();
        prueba.setVisible(true);
    }
}



