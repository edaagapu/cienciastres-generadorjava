#!/usr/bin/env python3

from jinja2 import Template
import os

diccionario = {"localizacion":"setLocation(","color":"setBackground(","texto":"setText(","tamano":"setSize("}

def contenido(lista,palabra):
    for i in lista:
        if palabra in i:
            return True
    return False

def construccion():
    os.system("ls -d */ > constructor.txt")
    fil = open("constructor.txt","r")
    archivos = []    
    carpetas = fil.read().split("\n")
    carpetas.pop()    
    fil.close()
    clases = []
    for i in range (0,len(carpetas)):
        os.system("ls "+carpetas[i]+ " > "+carpetas[i]+"archivos.txt")
        archivos.append(open(carpetas[i]+"archivos.txt","r"))
    for i in archivos:
        temporal = i.read().split("\n")
        temporal.pop(0)
        temporal.pop()        
        i.close()
        clases.append(temporal[:])
    for i in range(0,len(carpetas)):
        escribir = open(carpetas[i][:len(carpetas[i])-1]+".const","w")        
        for j in clases[i]:
            resultado=""
            propiedades=open(carpetas[i]+j,"r")
            prp = propiedades.read().split("\n")
            prp.pop()            
            padre = "contentPane.add("
            pre  = ""
            if not j[0].isupper():
                tempo = j.split(".")                
                pre = tempo[0]+"."          
            else:
                padre = ""            
            claves = []
            for k in prp:
                claves.append(k.split(":"))
                if "padre" in claves[len(claves)-1]:
                    padre = "pnl"+claves[len(claves)-1][1].capitalize()+".add("
                    claves.pop()
            if len(padre)!=0:            
                resultado = padre+pre[0:len(pre)-1]+");\n\t\t"
            for k in claves:
                if k[0]=="tamano" or k[0]=="localizacion":                
                    resultado = resultado + pre + diccionario[k[0]] + k[1].replace("-",",") +");\n\t\t"
                else:
                    resultado = resultado + pre + diccionario[k[0]] + k[1].replace("-",",") +");\n\t\t"                    
            escribir.write(resultado)
        escribir.close()
    os.system("rm constructor.txt")
construccion()

